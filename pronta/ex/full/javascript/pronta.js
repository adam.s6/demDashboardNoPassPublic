//create our editable grid
var editableGrid = new EditableGrid("DemoGridFull", {
	enableSort: true, // true is the default, set it to false if you don't want sorting to be enabled
	editmode: "absolute", // change this to "fixed" to test out editorzone, and to "static" to get the old-school mode
	editorzoneid: "edition", // will be used only if editmode is set to "fixed"
	pageSize: 50,
	maxBars: 10
});

//helper function to display a message
function displayMessage(text, style) { 
	_$("message").innerHTML = "<p class='" + (style || "ok") + "'>" + text + "</p>"; 
} 

//helper function to get path of a demo image
function image(relativePath) {
	return "images/" + relativePath;
}

//this will be used to render our table headers
function InfoHeaderRenderer(message) { 
	this.message = message; 
	this.infoImage = new Image();
	this.infoImage.src = image("information.png");
};

InfoHeaderRenderer.prototype = new CellRenderer();
InfoHeaderRenderer.prototype.render = function(cell, value) 
{
	if (value) {
		// here we don't use cell.innerHTML = "..." in order not to break the sorting header that has been created for us (cf. option enableSort: true)
		var link = document.createElement("a");
		link.href = "javascript:alert('" + this.message + "');";
		link.appendChild(this.infoImage);
		cell.appendChild(document.createTextNode("\u00a0\u00a0"));
		cell.appendChild(link);
	}
};

//this function will initialize our editable grid
EditableGrid.prototype.initializeGrid = function() 
{
	with (this) {

		// register the function that will handle model changes
		modelChanged = function(rowIndex, columnIndex, oldValue, newValue, row) { 
			displayMessage("Value for '" + this.getColumnName(columnIndex) + "' in row " + this.getRowId(rowIndex) + " has changed from '" + oldValue + "' to '" + newValue + "'");
			// if (this.getColumnName(columnIndex) == "continent") this.setValueAt(rowIndex, this.getColumnIndex("country"), ""); // if we changed the continent, reset the country
			this.renderCharts();
			if (newValue != oldValue)
            	sendChanges(columnIndex, rowIndex, newValue);
		};

		//wyslij zmiany
		sendChanges = function(columnIndex, rowIndex, newValue) {
			let collumn = "";
			let prontoId = "";
			if (columnIndex == 4) {
                prontoId = editableGrid.data[rowIndex]['columns'][1]
                console.log("comment" + prontoId + " " + newValue);
                collumn = "comment";
            }
            else if (columnIndex == 5) {
                prontoId = editableGrid.data[rowIndex]['columns'][1]
                console.log("prio" + prontoId + " " + newValue);
                collumn = "prio";
            }
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    console.log("wyslano" + columnIndex);
                }
            };
            xhttp.open("POST", "datasource/sendData.php", true);
            xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            console.log("collumn=" + collumn + "&prontoid=" + prontoId + "&value=" + newValue);
            xhttp.send("collumn=" + collumn + "&prontoid=" + prontoId + "&value=" + newValue);
		};

		// update paginator whenever the table is rendered (after a sort, filter, page change, etc.)
		tableRendered = function() { this.updatePaginator(); initializeColumnsFromStorage(); };

		// update charts when the table is sorted or filtered
		tableFiltered = function() { this.renderCharts(); };
		tableSorted = function() { this.renderCharts(); };

		rowSelected = function(oldRowIndex, newRowIndex) {
			if (oldRowIndex < 0) displayMessage("Selected row '" + this.getRowId(newRowIndex) + "'");
			else displayMessage("Selected row has changed from '" + this.getRowId(oldRowIndex) + "' to '" + this.getRowId(newRowIndex) + "'");
		};

		rowRemoved = function(oldRowIndex, rowId) {
			displayMessage("Removed row '" + oldRowIndex + "' - ID = " + rowId);
		};

		// // render for the action column
		// setCellRenderer("action", new CellRenderer({render: function(cell, value) {
		// 	// this action will remove the row, so first find the ID of the row containing this cell
		// 	var rowId = editableGrid.getRowId(cell.rowIndex);
        //
		// 	cell.innerHTML = "<a onclick=\"if (confirm('Are you sure you want to delete this person ? ')) { editableGrid.remove(" + cell.rowIndex + "); editableGrid.renderCharts(); } \" style=\"cursor:pointer\">" +
		// 	"<img src=\"" + image("delete.png") + "\" border=\"0\" alt=\"delete\" title=\"Delete row\"/></a>";
        //
		// 	cell.innerHTML+= "&nbsp;<a onclick=\"editableGrid.duplicate(" + cell.rowIndex + ");\" style=\"cursor:pointer\">" +
		// 	"<img src=\"" + image("duplicate.png") + "\" border=\"0\" alt=\"duplicate\" title=\"Duplicate row\"/></a>";
        //
		// }}));

		// render the grid (parameters will be ignored if we have attached to an existing HTML table)
		renderGrid("tablecontent", "testgrid", "tableid");

		// set active (stored) filter if any
		_$('filter').value = currentFilter ? currentFilter : '';

		// filter when something is typed into filter
		_$('filter').onkeyup = function() { editableGrid.filter(_$('filter').value); };

		// bind page size selector
		$("#pagesize").val(pageSize).change(function() { editableGrid.setPageSize($("#pagesize").val()); });
		$("#barcount").val(maxBars).change(function() { editableGrid.maxBars = $("#barcount").val(); editableGrid.renderCharts(); });
	}
};

EditableGrid.prototype.onloadJSON = function(url)
{
	// register the function that will be called when the XML has been fully loaded
	this.tableLoaded = function() { 
		displayMessage("Grid loaded from JSON: " + this.getRowCount() + " row(s)");
        document.getElementById("tytul").innerHTML = "DEM - Wailling Wall - Welcome! - Now we have " + this.getRowCount() + " prontos on DEM";
		this.initializeGrid();
		initializeColumnsFromStorage();
	};

	// load JSON URL
	this.loadJSON(url);
};

EditableGrid.prototype.duplicate = function(rowIndex)
{
	// copy values from given row
	var values = this.getRowValues(rowIndex);
	values['name'] = values['name'] + ' (copy)';

	// get id for new row (max id + 1)
	var newRowId = 0;
	for (var r = 0; r < this.getRowCount(); r++) newRowId = Math.max(newRowId, parseInt(this.getRowId(r)) + 1);

	// add new row
	this.insertAfter(rowIndex, newRowId, values); 
};

//function to render our two demo charts
EditableGrid.prototype.renderCharts = function()
{
	// this.renderBarChart("barchartcontent", 'Age per person' + (this.getRowCount() <= this.maxBars ? '' : ' (first ' + this.maxBars + ' rows out of ' + this.getRowCount() + ')'), 'name', { limit: this.maxBars, bar3d: false, rotateXLabels: this.maxBars > 10 ? 270 : 0 });
	// this.renderPieChart("piechartcontent", 'Country distribution', 'country', 'country');
};

//function to render the paginator control
EditableGrid.prototype.updatePaginator = function()
{
	var paginator = $("#paginator").empty();
	var nbPages = this.getPageCount();

	// get interval
	var interval = this.getSlidingPageInterval(20);
	if (interval == null) return;

	// get pages in interval (with links except for the current page)
	var pages = this.getPagesInInterval(interval, function(pageIndex, isCurrent) {
		if (isCurrent) return "" + (pageIndex + 1);
		return $("<a>").css("cursor", "pointer").html(pageIndex + 1).click(function(event) { editableGrid.setPageIndex(parseInt($(this).html()) - 1); });
	});

	// "first" link
	var link = $("<a>").html("<img src='" + image("gofirst.png") + "'/>&nbsp;");
	if (!this.canGoBack()) link.css({ opacity : 0.4, filter: "alpha(opacity=40)" });
	else link.css("cursor", "pointer").click(function(event) { editableGrid.firstPage(); });
	paginator.append(link);

	// "prev" link
	link = $("<a>").html("<img src='" + image("prev.png") + "'/>&nbsp;");
	if (!this.canGoBack()) link.css({ opacity : 0.4, filter: "alpha(opacity=40)" });
	else link.css("cursor", "pointer").click(function(event) { editableGrid.prevPage(); });
	paginator.append(link);

	// pages
	for (p = 0; p < pages.length; p++) paginator.append(pages[p]).append(" | ");

	// "next" link
	link = $("<a>").html("<img src='" + image("next.png") + "'/>&nbsp;");
	if (!this.canGoForward()) link.css({ opacity : 0.4, filter: "alpha(opacity=40)" });
	else link.css("cursor", "pointer").click(function(event) { editableGrid.nextPage(); });
	paginator.append(link);

	// "last" link
	link = $("<a>").html("<img src='" + image("golast.png") + "'/>&nbsp;");
	if (!this.canGoForward()) link.css({ opacity : 0.4, filter: "alpha(opacity=40)" });
	else link.css("cursor", "pointer").click(function(event) { editableGrid.lastPage(); });
	paginator.append(link);
};

function initializeColumnsFromStorage() {
    function initializeColumnFromStorage(col){
        let columnClass =  ".editablegrid-" + col;
        if (localStorage.getItem(col) == "1") {
            $(columnClass).show();
            document.getElementById(col).style.backgroundColor = "#8fbc8f";
        }
        else {
            $(columnClass).hide();
            document.getElementById(col).style.backgroundColor = "#dddddd";
        }
    }
    initializeColumnFromStorage("faultAnalysisId");
    initializeColumnFromStorage("problemReportId");
    initializeColumnFromStorage("team");
    initializeColumnFromStorage("developer");
    initializeColumnFromStorage("comment");
    initializeColumnFromStorage("priority");
    initializeColumnFromStorage("title");
    initializeColumnFromStorage("lastTransferTime");
    initializeColumnFromStorage("reportedDate");
    initializeColumnFromStorage("state");
    initializeColumnFromStorage("severity");
    initializeColumnFromStorage("groupInCharge");
    initializeColumnFromStorage("release");
    initializeColumnFromStorage("additional");
    initializeColumnFromStorage("topImportance");
    initializeColumnFromStorage("rdInformation");
    initializeColumnFromStorage("subsystem");
    initializeColumnFromStorage("optional");
    initializeColumnFromStorage("correctionsStatus");
    initializeColumnFromStorage("corrections");
    initializeColumnFromStorage("stateChangedtoFCC");
}
