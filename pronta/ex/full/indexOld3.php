<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>DEM - Wailling Wall</title>
		
		<!-- include javascript and css files for the EditableGrid library -->
		<script src="../../editablegrid.js"></script>
		<!-- [DO NOT DEPLOY] --> <script src="../../editablegrid_renderers.js" ></script>
		<!-- [DO NOT DEPLOY] --> <script src="../../editablegrid_editors.js" ></script>
		<!-- [DO NOT DEPLOY] --> <script src="../../editablegrid_validators.js" ></script>
		<!-- [DO NOT DEPLOY] --> <script src="../../editablegrid_utils.js" ></script>
		<!-- [DO NOT DEPLOY] --> <script src="../../editablegrid_charts.js" ></script>
		<link rel="stylesheet" href="../../editablegrid.css" type="text/css" media="screen">

		<!-- include javascript and css files for jQuery, needed for the datepicker and autocomplete extensions -->
		<script src="../../extensions/jquery/jquery-1.6.4.min.js" ></script>
		<script src="../../extensions/jquery/jquery-ui-1.8.16.custom.min.js" ></script>
		<link rel="stylesheet" href="../../extensions/jquery/jquery-ui-1.8.16.custom.css" type="text/css" media="screen">
		
		<!-- include javascript and css files for the autocomplete extension -->
		<script src="../../extensions/autocomplete/autocomplete.js" ></script>
		<link rel="stylesheet" href="../../extensions/autocomplete/autocomplete.css" type="text/css" media="screen">

		<!-- Uncomment this if you want to use the first variant of the autocomplete instead of the official one from jQuery UI -->
		<!--
		<script src="../../extensions/autocomplete_variant_1/jquery.autocomplete.min.js" ></script>
		<script src="../../extensions/autocomplete_variant_1/autocomplete.js" ></script>
		<link rel="stylesheet" href="../../extensions/autocomplete_variant_1/jquery.autocomplete.css" type="text/css" media="screen">
		!-->

		<!-- Uncomment this if you want to use the second variant of the autocomplete instead of the official one from jQuery UI -->
		<!--
		<script src="../../extensions/autocomplete_variant_2/jquery.autocomplete.min.js" ></script>
		<script src="../../extensions/autocomplete_variant_2/autocomplete.js" ></script>
		<link rel="stylesheet" href="../../extensions/autocomplete_variant_2/jquery.autocomplete.css" type="text/css" media="screen">
		!-->

		<!-- include javascript file for the Highcharts library -->
		<script src="../../extensions/Highcharts-4.0.4/js/highcharts.js"></script>

		<!-- include javascript and css files for this demo -->
		<script src="javascript/pronta.js" ></script>
		<link rel="stylesheet" type="text/css" href="css/pronta.css" media="screen"/>

		<script type="text/javascript">window.onload = function() { editableGrid.onloadJSON("datasource/pronta.php"); } </script>
	</head>
	
	<body>
		<div id="wrap">
		<h1 id="tytul">DEM - Wailling Wall - Welcome! </h1>
		
			<!-- Feedback message zone -->
			<div id="message" hidden></div>

			<!--  Number of rows per page and bars in chart -->
			<div id="pagecontrol">
				<label for="pagecontrol">Rows per page: </label>
				<select id="pagesize" name="pagesize">
					<option value="5">5</option>
					<option value="10">10</option>
					<option value="15">15</option>
					<option value="20">20</option>
					<option value="25">25</option>
					<option value="30">30</option>
					<option value="40">40</option>
					<option value="50">50</option>
				</select>
			</div>
		
			<!-- Grid filter -->
			<label for="filter">Filter :</label>
			<input type="text" id="filter"/>

            <style>
                .ustawiacz td,
                .ustawiacz th{
                    border: 0px solid #dddddd;
                    text-align: left;
                    padding: 8px;
                    background-color: #dddddd;
                    border-spacing: 2px;

                }

                td.editablegrid-comment td{
                    min-width: 200px;
                    -webkit-column-width: 200px;
                    -moz-column-width: 200px;
                    column-width: 200px;
                }


            </style>

            <div id="ustawiacz">
                <table class="ustawiacz">
                    <tr>
                        <td id="faultAnalysisId">faultAnalysisId</td>
                        <td id="problemReportId">problemReportId</td>
                        <td id="team">team</td>
                        <td id="developer">developer</td>
                        <td id="comment">comment</td>
                        <td id="priority">priority</td>
                        <td id="title">title</td>
                        <td id="lastTransferTime">lastTransferTime</td>
                        <td id="reportedDate">reportedDate</td>
                        <td id="state">state</td>
                        <td id="severity">severity</td>
                    </tr>
                    <tr>
                        <td id="groupInCharge">groupInCharge</td>
                        <td id="release">release</td>
                        <td id="additional">additional</td>
                        <td id="topImportance">topImportance</td>
                        <td id="rdInformation">rdInformation</td>
                        <td id="subsystem">subsystem</td>
                        <td id="optional">optional</td>
                        <td id="correctionsStatus">correctionsStatus</td>
                        <td id="corrections">corrections</td>
                        <td id="stateChangedtoFCC">stateChangedtoFCC</td>
                        <td id="xxx"></td>
                    </tr>
                </table>
            </div>

            <script>
                $(document).ready(function(){
                    if (localStorage.getItem("faultAnalysisId") === null)
                        localStorage.setItem("faultAnalysisId", "1");
                    if (localStorage.getItem("problemReportId") === null)
                        localStorage.setItem("problemReportId", "1");
                    if (localStorage.getItem("team") === null)
                        localStorage.setItem("team", "1");
                    if (localStorage.getItem("developer") === null)
                        localStorage.setItem("developer", "1");
                    if (localStorage.getItem("comment") === null)
                        localStorage.setItem("comment", "1");
                    if (localStorage.getItem("priority") === null)
                        localStorage.setItem("priority", "1");
                    if (localStorage.getItem("title") === null)
                        localStorage.setItem("title", "1");
                    if (localStorage.getItem("lastTransferTime") === null)
                        localStorage.setItem("lastTransferTime", "1");
                    if (localStorage.getItem("reportedDate") === null)
                        localStorage.setItem("reportedDate", "1");
                    if (localStorage.getItem("state") === null)
                        localStorage.setItem("state", "1");
                    if (localStorage.getItem("severity") === null)
                        localStorage.setItem("severity", "1");
                    if (localStorage.getItem("groupInCharge") === null)
                        localStorage.setItem("groupInCharge", "1");
                    if (localStorage.getItem("release") === null)
                        localStorage.setItem("release", "1");
                    if (localStorage.getItem("additional") === null)
                        localStorage.setItem("additional", "1");
                    if (localStorage.getItem("topImportance") === null)
                        localStorage.setItem("topImportance", "1");
                    if (localStorage.getItem("rdInformation") === null)
                        localStorage.setItem("rdInformation", "0");
                    if (localStorage.getItem("subsystem") === null)
                        localStorage.setItem("subsystem", "1");
                    if (localStorage.getItem("optional") === null)
                        localStorage.setItem("optional", "1");
                    if (localStorage.getItem("correctionsStatus") === null)
                        localStorage.setItem("correctionsStatus", "0");
                    if (localStorage.getItem("corrections") === null)
                        localStorage.setItem("corrections", "1");
                    if (localStorage.getItem("stateChangedtoFCC") === null)
                        localStorage.setItem("stateChangedtoFCC", "1");


                    function getFunctionHiddingColumn(col){
                        let className = ".editablegrid-" + col;
                        if (localStorage.getItem(col) == "1") {
                            localStorage.setItem(col, "0");
                            $(className).hide();
                            document.getElementById(col).style.backgroundColor = "#dddddd";
                        }
                        else {
                            localStorage.setItem(col, "1");
                            $(className).show();
                            document.getElementById(col).style.backgroundColor = "#8fbc8f";
                        }
                    };

                    faultAnalysisId.onclick = function() {getFunctionHiddingColumn("faultAnalysisId")};
                    problemReportId.onclick = function() {getFunctionHiddingColumn("problemReportId")};
                    team.onclick = function() {getFunctionHiddingColumn("team")};
                    developer.onclick = function() {getFunctionHiddingColumn("developer")};
                    comment.onclick = function() {getFunctionHiddingColumn("comment")};
                    priority.onclick = function() {getFunctionHiddingColumn("priority")};
                    title.onclick = function() {getFunctionHiddingColumn("title")};
                    lastTransferTime.onclick = function() {getFunctionHiddingColumn("lastTransferTime")};
                    reportedDate.onclick = function() {getFunctionHiddingColumn("reportedDate")};
                    state.onclick = function() {getFunctionHiddingColumn("state")};
                    severity.onclick = function() {getFunctionHiddingColumn("severity")};
                    groupInCharge.onclick = function() {getFunctionHiddingColumn("groupInCharge")};
                    release.onclick = function() {getFunctionHiddingColumn("release")};
                    additional.onclick = function() {getFunctionHiddingColumn("additional")};
                    topImportance.onclick = function() {getFunctionHiddingColumn("topImportance")};
                    rdInformation.onclick = function() {getFunctionHiddingColumn("rdInformation")};
                    subsystem.onclick = function() {getFunctionHiddingColumn("subsystem")};
                    optional.onclick = function() {getFunctionHiddingColumn("optional")};
                    correctionsStatus.onclick = function() {getFunctionHiddingColumn("correctionsStatus")};
                    corrections.onclick = function() {getFunctionHiddingColumn("corrections")};
                    stateChangedtoFCC.onclick = function() {getFunctionHiddingColumn("stateChangedtoFCC")};
                });
            </script>

			<!-- Grid contents -->
			<div id="tablecontent"></div>
		
			<!-- Paginator control -->
			<div id="paginator"></div>
		
			<!-- Edition zone (to demonstrate the "fixed" editor mode) -->
			<div id="edition"></div>
			
			<!-- Charts zone -->
<!--			<div id="barchartcontent"></div>-->
<!--			<div id="piechartcontent"></div>-->
			
		</div>
	</body>

</html>
