<?php

// This PHP script demonstrates how to generate XML grid data "on-the-fly"
// To achieve this, here we use our simple "PHP wrapper class" EditableGrid.php, but this is not mandatory.
// The only thing is that the generated XML must have the expected structure .
// Here we get the data from a CSV file; in real life, these data would probably come from a database.

require_once("../../../php/EditableGrid.php");

if (!$link = mysql_connect('localhost', 'pronta', 'xxx')) {
    echo 'Nie można nawiązać połączenia z bazą danych';
    exit;
}

if (!mysql_select_db('mydb', $link)) {
    echo 'Nie można wybrać bazy danych';
    exit;
}

// create grid and declare its columns
$grid = new EditableGrid();

// add two "string" columns
// if you wish you can specify the desired length of the text edition field like this: string(24)
$grid->addColumn("faultAnalysisId", "FA", "string", null, false);
$grid->addColumn("problemReportId", "PR", "url", "fdhtdj", false);
$grid->addColumn("team", "team", "string", null, false);
$grid->addColumn("developer", "developer", "string", null, false);
$grid->addColumn("comment", "comment", "string");
$grid->addColumn("priority", "prio", "integer");
$grid->addColumn("title", "title", "string", null, false);
$grid->addColumn("lastTransferTime", "transferred", "string", null, false);
$grid->addColumn("reportedDate", "reportedDate", "string", null, false);
$grid->addColumn("state", "state", "string", null, false);
$grid->addColumn("severity", "severity", "string", null, false);
$grid->addColumn("groupInCharge", "groupInCharge", "string", null, false);
$grid->addColumn("release", "rel", "string", null, false);
$grid->addColumn("additional", "additional", "string", null, false);
$grid->addColumn("topImportance", "topImportance", "string", null, false);
$grid->addColumn("rdInformation", "rdInformation", "string", null, false);
$grid->addColumn("subsystem", "subsystem", "string", null, false);
$grid->addColumn("optional", "optional", "string", null, false);
$grid->addColumn("correctionsStatus", "correctionsStatus", "string", null, false);
$grid->addColumn("corrections", "corrections", "string", null, false);
$grid->addColumn("stateChangedtoFCC", "stateChangedtoFCC", "string", null, false);


$data = array();
$fetch = mysql_query("SELECT * FROM pronto where transferred = 'in'");
$i = 0;
while ($row = mysql_fetch_array($fetch, MYSQL_ASSOC)) {
    if (count($row) <= 1 || $row[0] == 'id') continue;

    $data[] = array(
        "id" => $i,
        "problemReportId" => $row["problemReportId"],
        "comment" => $row["comment"],
        "priority" => $row["priority"],
        "faultAnalysisId" => $row["faultAnalysisId"],
		"title" => $row["title"],
        "lastTransferTime" => $row["lastTransferTime"],
        "reportedDate" => $row["reportedDate"],
		"state" => $row["state"],
		"severity" => $row["severity"],
		"groupInCharge" => $row["groupInCharge"],
		"release" => $row["releaseE"],
		"additional" => $row["additional"],
		"topImportance" => $row["topImportance"],
		"rdInformation" => $row["rdInformation"],
		"subsystem" => $row["subsystem"],
        "optional" => $row["optional"],
        "correctionsStatus" => $row["correctionsStatus"],
        "corrections" => $row["corrections"],
        "stateChangedtoFCC" => $row["stateChangedtoFCC"],
        "team" => $row["team"],
        "developer" => $row["developer"]
    );
    $i++;
}

// render XML or JSON
if (isset($_GET['xml'])) $grid->renderXML($data);
else $grid->renderJSON($data);
