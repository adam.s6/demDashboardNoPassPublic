<?php

if (!$link = mysql_connect('localhost', 'pronta', 'xxx')) {
    echo 'Nie można nawiązać połączenia z bazą danych';
    exit;
}

if (!mysql_select_db('mydb', $link)) {
    echo 'Nie można wybrać bazy danych';
    exit;
}

$after = $_GET["after"];
$before = $_GET["before"];

$fetch = mysql_query("select count(*) from action where time < '$before' and time >= '$after' and actionType = 'in'") or die(mysql_error());
$in = mysql_result($fetch, 0);

$fetch = mysql_query("select count(DISTINCT problemReportId) from action where time < '$before' and time >= '$after' and actionType = 'in'") or die(mysql_error());
$inUnique = mysql_result($fetch, 0);

$fetch = mysql_query("select count(*) from action where time < '$before' and time >= '$after' and actionType = 'out'") or die(mysql_error());
$out = mysql_result($fetch, 0);

$fetch = mysql_query("select count(DISTINCT problemReportId) from action where time < '$before' and time >= '$after' and actionType = 'out'") or die(mysql_error());
$outUnique = mysql_result($fetch, 0);

$fetch = mysql_query("select count(a.problemReportId) from action a where a.time < '$before' and a.time >= '$after' and a.actionType = 'out' and (select p.state from pronto p where p.problemReportId = a.problemReportId) = 'Closed'") or die(mysql_error());
$outClosed = mysql_result($fetch, 0);

$fetch = mysql_query("select count(a.problemReportId) from action a where a.time < '$before' and a.time >= '$after' and a.actionType = 'out' and (select p.state from pronto p where p.problemReportId = a.problemReportId) = 'Correction Not Needed'") or die(mysql_error());
$outCnn = mysql_result($fetch, 0);

$fetch = mysql_query("select count(a.problemReportId) from action a where a.time < '$before' and a.time >= '$after' and a.actionType = 'out' and (select p.state from pronto p where p.subsystem != 'SOAM_DEM' and p.problemReportId = a.problemReportId) != 'Closed'") or die(mysql_error());
$outTransferred = mysql_result($fetch, 0);

$fetch = mysql_query("select count(distinct a.problemReportId) from action a where a.time < '$before' and a.time >= '$after' and a.actionType = 'out' and (select p.state from pronto p where p.subsystem != 'SOAM_DEM' and p.problemReportId = a.problemReportId) != 'Closed'") or die(mysql_error());
$outTransferredUnique = mysql_result($fetch, 0);

$fetch = mysql_query("select count(a.problemReportId) from action a where a.time < '$before' and a.time >= '$after' and a.actionType = 'out' and (select p.state from pronto p where  p.subsystem = 'SOAM_DEM' and p.problemReportId = a.problemReportId) != 'Closed'") or die(mysql_error());
$outAttached = mysql_result($fetch, 0);

$outByDev = mysql_query(
    "select count(x.dev) as cou, x.dev from 
               (select distinct problemReportId, 
                   (select p.developer from pronto p where  p.problemReportId = a.problemReportId) as dev, 
                   (select pp.transferred from pronto pp where  pp.problemReportId = a.problemReportId) as transferred 
           from action a where a.time < '$before' and a.time >= '$after' and a.actionType = 'out') as x 
           where x.transferred = \"out\" group by x.dev") or die(mysql_error());

$obd = array();
while ($row = mysql_fetch_assoc($outByDev)) {
    $obd[] = array(
        "dev" => $row['dev'],
        "count" => $row['cou']
    );
}

$data->in = $in;
$data->inUnique = $inUnique;
$data->out = $out;
$data->outUnique = $outUnique;
$data->outClosed = $outClosed;
$data->outCnn = $outCnn;
$data->outTransferred = $outTransferred;
$data->outTransferredUnique = $outTransferredUnique;
$data->outAttached = $outAttached;
$data->outByDev = $obd;

header('Content-Type: application/json');
echo json_encode($data, JSON_PRETTY_PRINT);


